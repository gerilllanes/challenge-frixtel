import Grid from '@material-ui/core/Grid';
import Character from '../Character/Character'
import Container from '@material-ui/core/Container';


const CharactersList = ({ characters }) => {
    
    return (
        <Container maxWidth='lg' style={{marginTop: '2%'}} disableGutters>
            <Grid container spacing={4} >
                {
                    characters.map((character) => (
                            <Grid item sm={6} xs={12} lg={4} key={character.id}>
                                <Character character={character} />
                            </Grid>
                    ))
                }
            </Grid>
        </Container>
    )
}

export default CharactersList