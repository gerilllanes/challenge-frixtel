import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import IconButton from '@material-ui/core/IconButton';
import { useHistory } from "react-router-dom";
import { Button } from '@material-ui/core';



const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
        color: 'black',
        backgroundColor: '#E8E8E8',
    },
    searchButton: {
        marginRight: theme.spacing(2)
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    navbar: {
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'space-between',
    },
    logo: {
        width: '100px',
        height: '50px',
        marginRight: '3px'
    },
    favoritesIcon: {
        color: '#B700C2'
    },
}));

const Navbar = ({ setCharacterValue, getCharacters }) => {
    const classes = useStyles();

    let history = useHistory();

    const goFavorites = () => {
        history.push('/favorites')
    }

    const goHome = () => {
        history.push('/')
    }
    
    
    return (
        <div className={classes.grow}>
            <AppBar position="static">
                <Toolbar className={classes.navbar}>
                    <img src='https://1000marcas.net/wp-content/uploads/2020/02/Logo-Marvel.png' className={classes.logo} onClick={() => goHome()} alt='logo'/>
                    <div className={classes.search}>
                        <IconButton onClick={() => getCharacters()} className={classes.searchButton}>
                            <SearchIcon  />
                        </IconButton>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                            onChange={(e) => setCharacterValue(e.target.value)}
                        />
                    </div>
                    <IconButton onClick={() => goFavorites()}>
                        <StarBorderIcon className={classes.favoritesIcon} />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navbar