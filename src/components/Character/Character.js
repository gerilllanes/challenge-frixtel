import {useState, useContext} from 'react'

import './Character.css'

import StarBorderIcon from '@material-ui/icons/StarBorder';
import IconButton from '@material-ui/core/IconButton';

import ComicsList from '../ComicsList/ComicsList'

import {CharacterContext} from '../../Context/CharactersContext'

const Character = (props) => {
    const [openDialog, setOpenDialog] = useState(false)
    const [, dispatch] = useContext(CharacterContext)


    const openComicsDialog = () => {
        setOpenDialog(true)
    }

    const closeComicsDialog = () => {
        setOpenDialog(false)
    }

    const addToFavoritesList = () => {
        const newFavoriteCharacter = JSON.parse(localStorage.getItem('favorites')) || []
        newFavoriteCharacter.push(props.character)
        dispatch({type: 'addFavoriteCharacters', value: newFavoriteCharacter})
    }

    return (
        <div className='container'>
            <img src={props.character.thumbnail.path + '.' + props.character.thumbnail.extension} className='image' onClick={() => openComicsDialog()} alt='img'/>
            <div className='add-favorites-button'>
                <IconButton onClick={() => addToFavoritesList()}>
                    <StarBorderIcon className='add-favorites-button-icon' fontSize="large"/>
                </IconButton>
            </div>
            <div className='heroe-name'>
                <p style={{marginTop: '3px', marginBottom: '4px', textAlign: 'center'}}>
                    {props.character.name}
                </p>
            </div>

            <ComicsList open={openDialog} comics={props.character.comics} characterName={props.character.name} closeComicsDialog={closeComicsDialog}/>
        </div>
    )
}

export default Character