import "./App.css";
import {
  CharacterProvider,
  initialState,
  reducer,
} from "./Context/CharactersContext";
import Home from "./screens/Home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Favorites from "./screens/Favorites/Favorites";

function App() {
  return (
    <CharacterProvider initialState={initialState} reducer={reducer}>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/favorites" component={Favorites} />
        </Switch>
      </Router>
    </CharacterProvider>
  );
}

export default App;
