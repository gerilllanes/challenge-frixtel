import { useEffect, useState } from "react";

import Navbar from "../../components/Navbar/Navbar";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Character from "../../components/Character/Character";

const Favorites = () => {
  const [favoriteCharacters, setFavoriteCharacters] = useState(null);

  useEffect(() => {
    const characters = JSON.parse(localStorage.getItem("favorites")) || null;
    setFavoriteCharacters(characters);
  }, []);

  return (
    <>
      <Navbar />
      <Container maxWidth="lg" style={{ marginTop: "2%" }} disableGutters>
        <Grid container spacing={4}>
          {favoriteCharacters !== null ? (
            favoriteCharacters.map((character) => (
              <Grid item sm={6} xs={12} lg={4}>
                <Character character={character} />
              </Grid>
            ))
          ) : (
            <h3>No hay personajes favoritos.</h3>
          )}
        </Grid>
      </Container>
    </>
  );
};

export default Favorites;
