import { useEffect, useContext } from "react";

import Navbar from "../../components/Navbar/Navbar";
import CharactersList from "../../components/CharactersList/CharactersList";
import axios from "axios";
import notFound from '../../assets/notfound.png'

//CONTEXT
import { CharacterContext } from "../../Context/CharactersContext";

const Home = () => {

  const [{ search, characters }, dispatch] = useContext(CharacterContext);

  const getCharacters = async () => {
    const char = search ?
    await axios.get(
        `https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=${search}&ts=1&apikey=8b029c38722c5974a9352c095e0bbf6c&hash=234079163455dc964e9863bfcd3be354`
    ) :
    await axios.get(
        `https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=8b029c38722c5974a9352c095e0bbf6c&hash=234079163455dc964e9863bfcd3be354`
    );
    const charList = char.data.data.results.sort(()=> Math.random() - 0.5);
    dispatch({ type: "characters", value: charList });

  };

  const setCharacterValue = (character) => {
    dispatch({ type: "search", value: character });
  };

  useEffect(() => {
    getCharacters();
  }, []);

  return (
    <>
      <Navbar setCharacterValue={setCharacterValue} getCharacters={getCharacters}/>
      {
        characters.length ? 
        (
          <CharactersList characters={characters} />
        )
        : 
        (
          <div style={{display: 'flex', justifyContent: 'center', padding: '3%'}}>
            <img src={notFound} alt='not found'/>
          </div>
        )
      }
    </>
  );
};

export default Home;
