import {useReducer} from 'react'

import {createContext} from 'react'

export const initialState = {
    search: '',
    characters: [],
    favoriteCharacters: [],
    comicsCharacters: [],
}


export const CharacterContext = createContext()

export const reducer = (state, action) => {
    switch(action.type) {
        case 'search':
            return {
                ...state,
                search: action.value
            }
        case 'characters':
            return {
                ...state,
                characters: action.value
            }
        case 'addFavoriteCharacters':
            localStorage.setItem('favorites', JSON.stringify(action.value))
            return {
                ...state,
                favoriteCharacters: action.value
            }
    }
}

export const CharacterProvider = ({ children, initialState, reducer }) => (
    <CharacterContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </CharacterContext.Provider>
)